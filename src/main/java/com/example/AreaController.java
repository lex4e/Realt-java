package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.example.Area;
import com.example.AreaRepository;
import com.example.storage.StorageProperties;
import com.example.storage.StorageService;

@Controller   
@RequestMapping(path="/area")
public class AreaController {

	private final StorageService storageService;
	    
	@Autowired 
	private AreaRepository areaRepository;

	@Autowired
    public AreaController(StorageService storageService) {
        this.storageService = storageService;
    }
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
    public @ResponseBody String delete(@RequestParam Long id) {
		Area a = areaRepository.findOne(id);
		areaRepository.delete(a);
		return "Deleted";
	}

	@GetMapping("/add")
    public String areaForm(Model model) {
        model.addAttribute("area", new Area());
        return "add";
    }

    @PostMapping("/add")
    public String areaSubmit(@ModelAttribute Area area, @RequestParam("file") MultipartFile file) {
    	storageService.store(file);
    	area.setImage(file.getOriginalFilename());
    	areaRepository.save(area);
        return "result";
    }

	@GetMapping("/edit")
    public String areaEditForm(@RequestParam Long id, Model model) {
        model.addAttribute("area", areaRepository.findOne(id));
        return "edit";
    }

    @PostMapping("/edit")
    public String areaEditSubmit(@ModelAttribute Area area, @RequestParam("file") MultipartFile file) {
    	storageService.store(file);
    	if (file != null) area.setImage(file.getOriginalFilename());
    	areaRepository.save(area);
        return "edit";
    }
    
	@GetMapping(path="/all")
	public String getAllAreas(Model model) {
		model.addAttribute("areas", areaRepository.findAll());
		return "list";
	}
	
	@GetMapping(path="/all/json")
	public @ResponseBody Iterable<Area> getAllAreas() {
		return areaRepository.findAll();
	}
}
