package com.example;

import org.springframework.data.repository.CrudRepository;
import com.example.Area;

public interface AreaRepository extends CrudRepository<Area, Long> {

}